import 'package:flutter/material.dart';
import 'package:image_explorer/pages/pages.dart';

import 'services/services.dart';
void main() async{
   WidgetsFlutterBinding.ensureInitialized();
 

  
  await HiveInstance.initializeHive();
 
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    if(Global.boxes[BOX_NAME.IMAGE_BOX]!.values.isEmpty){
  FetchImageService.repeatFetch();
    }
  
    return const MaterialApp(
      home: HomePage(key: Key('home_page'),),
    );
  }
}

