import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_explorer/models/home_page/home_page.dart';

import 'image_element.dart';
import 'image_search.dart';
class AlternateImageList extends StatefulWidget {
  const AlternateImageList({ Key? key, required this.images,}) : super(key: key);
final List<ImageView> images;


  @override
  State<AlternateImageList> createState() => _AlternateImageListState();
}

class _AlternateImageListState extends State<AlternateImageList> {
   TextEditingController textController = TextEditingController();
  PageController pageController =PageController(initialPage: 0,keepPage: true, viewportFraction: 1);
  double toPage = 0;
  @override
  void initState(){
   pageController.addListener(() {
      setState(() {
       toPage = pageController.page! ;
      });
    });
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
   
    
    return Stack(
      children: [
        PageView.builder(
         pageSnapping: false,
         physics:const PageScrollPhysics(),
          controller: pageController,
          scrollDirection: Axis.vertical,
          itemCount: widget.images.length,
          
          itemBuilder: (BuildContext context, int index){
          
          return  ImageElement(
                  key: const Key('image_element'),
                  url: widget.images[index].downloadUrl,
                  index: index,
                );
        }),
         ImageSearch(
          key: const Key('image_search'),
          textController: textController,
          search: () {
            
            textController.text = textController.text.trim();
           
            if (textController.text.isNotEmpty) {
              pageController.jumpToPage(int.parse(textController.text)-1);
               
             }

          
          },
        ),
      ],
    );
  }
}