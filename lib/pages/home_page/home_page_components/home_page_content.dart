import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:image_explorer/models/home_page/home_page.dart';
import 'package:image_explorer/pages/home_page/home_page_components/alternate_image_list.dart';
import 'package:image_explorer/services/global.dart';
import './home_page_components.dart';



class HomePageContent extends StatefulWidget {
  const HomePageContent({ Key? key }) : super(key: key);

  @override
  State<HomePageContent> createState() => _HomePageContentState();
}

class _HomePageContentState extends State<HomePageContent> {
  TextEditingController textController = TextEditingController();
  bool isSearching = false;
  String imageNumber = '';
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ValueListenableBuilder(
          valueListenable: Global.boxes[BOX_NAME.IMAGE_BOX]!.listenable(),
          builder: (BuildContext context,Box box,child) {
            if(box.isNotEmpty){
              List<ImageView> images = 
             List<ImageView>.from(box.values).toList();
              return AlternateImageList(
                
                images: images,
              );
            } else{
              return const Center(child: CupertinoActivityIndicator());
            }
            
          }
        ),
       
      ],
    );
  }
}
