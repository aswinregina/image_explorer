import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_explorer/services/global.dart';
import 'image_list_view.dart';

import 'package:image_explorer/models/home_page/image_view.dart' show ImageView;

class ImageList extends StatefulWidget {
  const ImageList(
      {Key? key, required this.imageNumber, required this.isSearching})
      : super(key: key);
  final String imageNumber;
  final bool isSearching;
  @override
  State<ImageList> createState() => _ImageListState();
}

class _ImageListState extends State<ImageList> {
  List<ImageView> originalItems = <ImageView>[];

  List<ImageView> items = <ImageView>[];
  ScrollController imageScrollController = ScrollController();
  int presentImage = 0;
  int imagePerPage = 30;
  bool isLoading = false;
  bool search = false;
  int toImage = 0;
  int lastImage = 0;
  @override
  void initState() {
    try {
      List tempItems = Global.boxes[BOX_NAME.IMAGE_BOX]!.values.toList();
      tempItems.forEach((element) {
        originalItems.add(element);
       
      });
       toImage = int.parse(widget.imageNumber);
    } catch (e) {
      toImage = 0;
      print('cant add list');
    }
    print('-----${originalItems.length}------');
    setState(() {
      items.addAll(
          originalItems.getRange(presentImage, presentImage + imagePerPage));
      presentImage = presentImage + imagePerPage;
    });
    super.initState();
  }

  addImages() async {
    await Future.delayed(const Duration(milliseconds: 150));

    print("load more");

    setState(() {
      if ((toImage) > originalItems.length) {
        items
            .addAll(originalItems.getRange(originalItems.length - imagePerPage, originalItems.length));
      } else if(lastImage>toImage){
        lastImage = lastImage - toImage;
        items.addAll(
            originalItems.getRange(toImage, lastImage));
      }else {
         items.addAll(
            originalItems.getRange(toImage, lastImage));
      }
      lastImage = toImage;
      toImage = toImage + imagePerPage;
      isLoading = false;
    });
  }
moveTo()async{
 await Future.delayed(const Duration(milliseconds: 150));

    print("load more");

    setState(() {
      if ((presentImage + imagePerPage) > originalItems.length) {
        items
            .addAll(originalItems.getRange(presentImage, originalItems.length));
      } else {
        items.addAll(
            originalItems.getRange(presentImage, presentImage + imagePerPage));
      }
      lastImage = presentImage;
      presentImage = presentImage + imagePerPage;
      isLoading = false;
    });
}
  removeImages() async {
    print('remove');
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification scrollInfo) {
        if (!isLoading &&
            scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          addImages();
          setState(() {
            isLoading = true;
            search = false;
          });
        }
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.minScrollExtent) {
          removeImages();
          search = false;
        }
        if (widget.isSearching) {
          addImages();
           setState(() {
            isLoading = true;
            search = false;
          });
        }
        return true;
      },
      child: ImageListView(
        imageScrollController: imageScrollController,
        items: items,
        isSearching: widget.isSearching,
        imageNumber: widget.imageNumber,
        isLoading: isLoading,
      ),
    );
  }
}
