import 'package:flutter/material.dart';
class ImageSearch extends StatelessWidget {
  const ImageSearch({ Key? key, required this.textController,required this.search}) : super(key: key);
final TextEditingController textController;
final VoidCallback search;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      child:TextFormField(
      //  onEditingComplete: search,
        controller: textController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          border: const OutlineInputBorder(borderSide: BorderSide.none),
          contentPadding: const EdgeInsets.all(16),
          hintText: 'search image number',
        
          suffix: 
               InkWell(
                  child:const Icon(
                   
                         Icons.search,
                       
                    color: Colors.black,
                  ),
                  onTap:search,
                  
                )
             ,
        ),
      )
    );
  }
}