import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:optimized_cached_image/optimized_cached_image.dart';
class ImageElement extends StatelessWidget {
  const ImageElement
({ Key? key, required this.url, required this.index}) : super(key: key);
final String url;
final int index;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('${index+1}'),
          Container(
             height: MediaQuery.of(context).size.width,
            width: MediaQuery.of(context).size.width,
            color: Colors.blueGrey,
         
            child: OptimizedCacheImage(imageUrl: url,placeholder: (context, url) => const CupertinoActivityIndicator(),
            imageBuilder: (context, imageProvider) {
                return Container(
                 
                  decoration: BoxDecoration(
                    borderRadius:const BorderRadius.all(Radius.circular(4)),
                    color: Colors.transparent,
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              },
              errorWidget: (context, url, error) => const Text('unable to load')),
          ),
        ],
      ),
    );
  }
}