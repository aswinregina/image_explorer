import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_explorer/models/home_page/image_view.dart';
import 'image_element.dart';

class ImageListView extends StatelessWidget {
  const ImageListView(
      {Key? key,
      required this.imageScrollController,
      required this.imageNumber,
      required this.isSearching,
      required this.items,
      required this.isLoading})
      : super(key: key);

  final ScrollController imageScrollController;
  final String imageNumber;
  final List<ImageView> items;
  final bool isSearching;
  final bool isLoading;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: imageScrollController,
      shrinkWrap: true,
      cacheExtent: 20000,
      itemBuilder: (BuildContext context, int index) {
        if (isSearching) {
          //imageScrollController.jumpTo()
          imageScrollController.animateTo((int.parse(imageNumber)) * 100,
              duration: const Duration(milliseconds: 200), curve: Curves.ease);
        }

        return
            Column(
          children: [
            ImageElement(
              key: const Key('image_element'),
              url: items[index].downloadUrl,
              index: index,
            ),
            if (isLoading) const CupertinoActivityIndicator()
          ],
        );
      },
      itemCount: items.length,
    );
  }
}
