import 'package:flutter/material.dart';
import './home_page_components/home_page_components.dart';
class HomePage extends StatelessWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(key: const Key('home_page_appBar'),
      title: const Text('Image Explorer'),
      ),
      body: const HomePageContent(),
    );
  }
}

