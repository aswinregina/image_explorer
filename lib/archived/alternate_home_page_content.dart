// class HomePageContent extends StatefulWidget {
//   const HomePageContent({
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<HomePageContent> createState() => _HomePageContentState();
// }

// class _HomePageContentState extends State<HomePageContent> {
//   TextEditingController textController = TextEditingController();
//   bool isSearching = false;
//   String imageNumber = '';
//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         Padding(
//           padding: const EdgeInsets.only(top: 24),
//           child: ValueListenableBuilder(
//             valueListenable: Global.boxes[BOX_NAME.IMAGE_BOX]!.listenable(),
//             builder: (BuildContext context,Box box,child) {
//               if(box.isNotEmpty){
//                 return ImageList(
//                 key: const Key('image_list'),
//                 imageNumber: textController.text,
//                 isSearching: isSearching,
//               );
//               } else{
//                 return const Center(child: CupertinoActivityIndicator());
//               }
              
//             }
//           ),
//         ),
//         ImageSearch(
//           key: const Key('image_search'),
//           textController: textController,
//           search: () {
//             textController.text = textController.text.trim();
//             if (textController.text.isNotEmpty) {
//               setState(() {
//                 isSearching = true;
              
//               });

//             }else {
//                setState(() {
//                 isSearching = false;
//               });
//             }
          
//           },
//         ),
//       ],
//     );
//   }
// }