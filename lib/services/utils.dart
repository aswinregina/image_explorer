import 'package:random_string/random_string.dart';
import 'dart:math'show Random;

class Utils{
  static String getSecureString(int length) {
    Random r = Random.secure();
    return randomAlphaNumeric(length, provider: CoreRandomProvider.from(r));
  }
}