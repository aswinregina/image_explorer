import 'package:hive/hive.dart' show Box;
import 'services.dart';
enum BOX_NAME {
  IMAGE_BOX
  }
  class Global{
    static  String hiveRepoStoragePath = 'image_explorer';
    static final Map<BOX_NAME, Box?> boxes = {
    BOX_NAME. IMAGE_BOX: HiveInstance.imageBox,};
  }