import 'dart:io';

import 'package:image_explorer/models/models.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'services.dart';
class HiveInstance {
  static Box? _imageBox;
  static Box? get imageBox => _imageBox;
  static initializeHive()async{

      Directory appDocDir = await getApplicationDocumentsDirectory();
    String hiveRepoPath = p.join(appDocDir.path, Global.hiveRepoStoragePath);
    await Hive.initFlutter(hiveRepoPath);
    Hive.registerAdapter(ImageAdapter());
     _imageBox = await Hive.openBox('image');
  }
  
  }