import 'dart:convert';

import 'package:image_explorer/models/models.dart';
import 'package:http/http.dart' as http;
import 'package:image_explorer/services/global.dart';
import 'package:image_explorer/services/utils.dart';

class FetchImageService {
  static fetchImage() async {
    final response = await http
        .get(Uri.parse('https://picsum.photos/v2/list?page=1&limit=100'));

    if (response.statusCode == 200) {
      List data = jsonDecode(response.body);
      for (var element in data) {
        String id = Utils.getSecureString(10);

        Global.boxes[BOX_NAME.IMAGE_BOX]!.put(id, ImageView.fromJson(element));
      }
    } else {
      throw Exception('Failed to load album');
    }
  }

  static repeatFetch() {
    for (var i = 0; i < 200; i++) {
      fetchImage();
    }
  }
}
