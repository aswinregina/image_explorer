import 'package:hive/hive.dart';
part 'image.g.dart';
@HiveType(typeId: 0)
class ImageView {
  @HiveField(0)
  String id;
  @HiveField(1)
  String author;
  @HiveField(2)
  int width;
  @HiveField(3)
  int height;
  @HiveField(4)
  String url;
  @HiveField(6)
  String downloadUrl;

  ImageView(
      {
       required this.id,
     required this.author,
     required this.width,
     required this.height,
     required this.url,
     required this.downloadUrl});

 factory ImageView.fromJson(Map<String, dynamic> json) {
   return ImageView( id : json['id'],
    author : json['author'],
    width : json['width'],
    height : json['height'],
    url : json['url'],
    downloadUrl :json['download_url']);
  }

  
}