// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_view.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ImageAdapter extends TypeAdapter<ImageView> {
  @override
  final int typeId = 0;

  @override
  ImageView read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ImageView(
      id: fields[0] as String,
      author: fields[1] as String,
      width: fields[2] as int,
      height: fields[3] as int,
      url: fields[4] as String,
      downloadUrl: fields[6] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ImageView obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.author)
      ..writeByte(2)
      ..write(obj.width)
      ..writeByte(3)
      ..write(obj.height)
      ..writeByte(4)
      ..write(obj.url)
      ..writeByte(6)
      ..write(obj.downloadUrl);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImageAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
